import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    randomResult: Array,
  },
  mutations: {
    setRandomResult(state, payload) {
      state.randomResult = payload;
    },
  },
  actions: {},
});
